import { type Role } from '$lib/auth'

export type User = {
	id: string
	name: string
	email: string
	holdingId: string | null
	companyId: string | null
	role: Role
}

export const USERS: User[] = [
	{
		id: '1',
		name: 'ADMIN FLORIPABIT',
		email: 'admin@floripabit.com.br',
		holdingId: null,
		companyId: null,
		role: 'ADMIN'
	},
	{
		id: '2',
		name: 'Alecrim Dourado',
		email: 'alecrim@floripabit.com.br',
		holdingId: '20',
		companyId: '30',
		role: 'MEMBER'
	}
]