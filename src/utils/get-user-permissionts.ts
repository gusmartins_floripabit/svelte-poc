import { defineAbilityFor } from '$lib/auth'

export function getUserPermissions(user: User) {
  const ability = defineAbilityFor(user)

  return ability
}