import { Files, LayoutDashboard, User, Building2, Building } from 'lucide-svelte'; 

type MenuItem = {
	label: string;
	href: string;
	IconComponent: unknown
};

export const menuItems: MenuItem[] = [
  {
    label: 'Dashboard',
    href: '/dashboard',
    IconComponent: LayoutDashboard
  },
  {
    label: 'Perfil',
    href: '/profile',
    IconComponent: User
  },
  {
    label: 'Holdings',
    href: '/holdings',
    IconComponent: Building2
  },
  {
    label: 'Empreendimentos',
    href: '/subsidiary',
    IconComponent: Building
  },
  {
    label: 'Contratadas',
    href: '/company',
    IconComponent: Building
  },
  {
    label: 'Relatórios',
    href: '/reports',
    IconComponent: Files
  }
]