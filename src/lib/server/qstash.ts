import { Client } from '@upstash/qstash'
import { QSTASH_TOKEN } from '$env/static/private'

const qstashClient = new Client({
  token: QSTASH_TOKEN
})

export { qstashClient }