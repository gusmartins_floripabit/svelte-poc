import { writable } from 'svelte/store'

const userStore = writable({} as User)

export { userStore }