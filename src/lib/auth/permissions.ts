import { AbilityBuilder } from "@casl/ability"
import type { AppAbility } from "./abilities"
import type { Role } from "./roles"

type PermissionsByRole = (
  user: User,
  builder: AbilityBuilder<AppAbility>
) => void

export const permissions: Record<Role, PermissionsByRole> = {
  ADMIN(_, { can }) {
    can('manage', 'all')
  },
  MEMBER(user, { can }) {
    can('get', 'Contracted')
  },
  HOLDING(_, { can }) {
    can('manage', 'all')
  }
}