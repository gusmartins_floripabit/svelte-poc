import { z } from 'zod'
import { holdingSchema } from '../models/holding'

export const holdingSubject = z.tuple([
  z.union([
    z.literal('manage'),
    z.literal('create'),
    z.literal('update'),
    z.literal('get')
  ]),
  z.union([z.literal('Holding'), holdingSchema])
])