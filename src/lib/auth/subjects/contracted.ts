import { z } from 'zod'
import { contractedSchema } from '../models/contracted'

export const contractedSubject = z.tuple([
  z.union([
    z.literal('manage'),
    z.literal('create'),
    z.literal('update'),
    z.literal('get')
  ]),
  z.union([z.literal('Contracted'), contractedSchema])
])