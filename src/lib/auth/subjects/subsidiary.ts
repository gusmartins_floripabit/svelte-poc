import { z } from 'zod'
import { subsidiarySchema } from '../models/subsidiary'

export const subsidiarydSubject = z.tuple([
  z.union([
    z.literal('manage'),
    z.literal('create'),
    z.literal('update'),
    z.literal('get')
  ]),
  z.union([z.literal('Subsidiary'), subsidiarySchema])
])