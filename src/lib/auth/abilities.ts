import { z } from 'zod'
import { holdingSubject } from './subjects/holding'
import { contractedSubject } from './subjects/contracted'
import { AbilityBuilder, createMongoAbility, type CreateAbility, type MongoAbility } from '@casl/ability'
import { permissions } from './permissions'
import { subsidiarydSubject } from './subjects/subsidiary'

const appAbilities = z.union([
  holdingSubject,
  contractedSubject,
  subsidiarydSubject,
  z.tuple([z.literal('manage'), z.literal('all')])
])

type AppAbilities = z.infer<typeof appAbilities>

export type AppAbility = MongoAbility<AppAbilities>
export const createAppAbility = createMongoAbility as CreateAbility<AppAbility>

export function defineAbilityFor(user: User): AppAbility {
  const builder = new AbilityBuilder(createAppAbility)

  if(typeof permissions[user.role] === 'function') {
    permissions[user.role](user, builder)
  } else {
    throw new Error(`Trying to use unknow role ${user.role}`)
  }

  const ability = builder.build({
    detectSubjectType(subject) {
      return subject.__typename
    },
  })

  ability.can = ability.can.bind(ability)
  ability.cannot = ability.cannot.bind(ability)

  return ability
}