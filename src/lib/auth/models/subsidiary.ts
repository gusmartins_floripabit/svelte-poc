import { z } from 'zod'

export const subsidiarySchema = z.object({
  __typename: z.literal('Subsidiary').default('Subsidiary'),
  id: z.string(),
  holdingId: z.string(),
})

export type Subsidiary = z.infer<typeof subsidiarySchema>