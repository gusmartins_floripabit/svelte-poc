import { z } from 'zod'

export const contractedSchema = z.object({
  __typename: z.literal('Contracted').default('Contracted'),
  id: z.string(),
  holdingId: z.string(),
  subsidiaryId: z.string()
})

export type Contracted = z.infer<typeof contractedSchema>