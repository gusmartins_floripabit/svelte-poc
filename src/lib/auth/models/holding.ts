import { z } from 'zod'

export const holdingSchema = z.object({
  __typename: z.literal('Holding').default('Holding'),
  id: z.string(),
})

export type Holding = z.infer<typeof holdingSchema>