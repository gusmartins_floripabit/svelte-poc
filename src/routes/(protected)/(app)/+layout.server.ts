import type { LayoutServerLoad } from './$types';

export const load: LayoutServerLoad = ({ cookies }) => {
  const user = cookies.get('user', {
    decode(value) {
      return JSON.parse(decodeURIComponent(value))
    },
  }) as string;

  return {
    user
  };
};