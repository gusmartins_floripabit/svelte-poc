import type { RequestHandler } from "@sveltejs/kit"
import { qstashClient } from "$lib/server/qstash";
import { DATALIFE_API } from "$env/static/private";

export const POST: RequestHandler = async () => {
  try {
    const bodyAuth = {
      username: 'qa@floripabit.com.br',
      password: 'Qafloripabit0*'
    };
    const responseAuthJSON = await fetch(DATALIFE_API, {
      method: 'POST',
      body: JSON.stringify(bodyAuth),
      headers: {
        'Content-type': 'application/json'
      },
    }).then((response) => response.json());

    const token = responseAuthJSON.token;

    await qstashClient.publishJSON({
      topic: 'INTERNAL_TOPIC',
      body: {
        deliveryTo: 'gustavosm994@gmail.com',
        payload: {
          filters: {
            //contractedId: "99",
            //clientId: "88",
            companie: '170',
            type: '0',
            notStatus: '4',
            salary: "true"
          },
          reportPathName: '/workers/reportListWorkersXls'
        }
      },
      headers: {
        'Content-type': 'application/json',
        'datalife-signature': token,
        'datalife-refresh-signature': 'refresh-token'
      },
    });

    return new Response()
  } catch (error) {
    console.log(error)
    throw new Error()
  }
}