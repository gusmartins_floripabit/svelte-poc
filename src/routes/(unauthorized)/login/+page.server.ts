import { type Actions, redirect } from '@sveltejs/kit';
import { USERS } from '../../../utils/users';

export const actions: Actions = {
	login: async ({ cookies, request }) => {
		const formData = await request.formData();
		const email = formData.get('email')

		const loggedUser = USERS.find((user) => user.email === email)
		if (!loggedUser) throw new Error('User does not exists')
		
		/**
		 * não pode salvar nos cookies o ROLE do usuário
		 * Depois tem que arrumar
		 */
		cookies.set('user', JSON.stringify(loggedUser), {
			path: '/',
			sameSite: 'strict',
			httpOnly: true,
			secure: true,
			maxAge: 60 * 60 * 24 * 30,
		});

		redirect(303, '/app/dashboard');
	}
}
