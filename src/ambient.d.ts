import { type Role } from '$lib/auth'

declare global {
  type User = {
    id: string
    name: string
    email: string
    holdingId: string | null
    companyId: string | null
    role: Role
  }
}